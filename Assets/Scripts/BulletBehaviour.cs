﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class BulletBehaviour : MonoBehaviour {
	public Rigidbody Projectile;
	public GameObject Floor;
	public Rigidbody Target;
	public Camera CamMain;
	public Camera CamFront;
	public Slider Velocity_Slider;
	public Slider Angle_Slider;


	public float Projectile_Size;
	public Text Vx_Text;
	public Text Vy_Text;
	public Text Height_Text;
	public Text Distance_Text;
	public Text Max_Height;
	public Text TimeInfo;


	public Text Angle_Info;
	public Text Speed_Info;

	public Text TargetDistance;

	private float Angle = 45;
	public float Mass = 10;
	private float Velocity_Mag = 100;


	private Vector3 InitialPosition;


	private float max_height = 0f;
	private bool moving = false;
	private bool Won = false;
	private Vector3 Velocity;

	private float Timer = 0.0f;
	// Use this for initialization
	void Start () {
		Projectile.transform.localScale = new Vector3(Projectile_Size, Projectile_Size, Projectile_Size);
		Target.transform.position = new Vector3(0f,0.3f,Projectile.transform.position.z + Random.Range(30,1000));
		Target.transform.localScale = new Vector3(4f,0.3f,4f);
		Projectile.mass = Mass;
		Projectile.useGravity = false;
		InitialPosition = Projectile.transform.position;
		Velocity = new Vector3(0, Velocity_Mag * Mathf.Sin(Mathf.Deg2Rad * Angle), Velocity_Mag * Mathf.Cos(Mathf.Deg2Rad * Angle));
		CamMain.enabled = true;
		CamFront.enabled = false;
	}
	// Update is called once per frame
	void Update () {
		Angle = Angle_Slider.value;
		Velocity_Mag = Velocity_Slider.value *100;

		if(Projectile.transform.position.y > max_height){
			max_height = Projectile.transform.position.y;
			//Debug.Log("Max Speed: " + max_height.ToString());
			Timer+=Time.deltaTime;
		}
		if(Input.GetKeyUp("c")){
			CamMain.enabled = !CamMain.enabled;
			CamFront.enabled = !CamFront.enabled;
		}

		if(Angle > 90){
			Angle=90;
		}
		if(Angle < 0){
			Angle = 0;
		}
		if(Velocity_Mag < 0){
			Velocity_Mag = 0;
		}
		if(Velocity_Mag>10000){
			Velocity_Mag = 10000;
		}
		CamFront.transform.position = new Vector3(0f,3f,Projectile.transform.position.z + 100);
		Velocity = new Vector3(0, Velocity_Mag * Mathf.Sin(Mathf.Deg2Rad * Angle), Velocity_Mag * Mathf.Cos(Mathf.Deg2Rad * Angle));

		if (!moving && Input.GetKeyUp("space")) {
			Projectile.AddForce(Velocity);
			//Debug.Log("Initial Velocity: " + Projectile.velocity.ToString());
			Projectile.useGravity = true;
			moving = true;
			Timer = 0.0f;
		}
		// if(Projectile.detectCollisions && moving){
		// 	var list = Projectile.GetComponents<Collider>();
		// 	foreach(var item in list){
		// 		//Debug.Log(item.ToString());
		// 		if(item.gameObject.name == "Target"){
		// 			Debug.Log("Collided With: " + Projectile.GetComponent<Collider>().gameObject.name + " at " + (Projectile.transform.position - InitialPosition).ToString());
		// 			Debug.Log("Collided target");
		// 			moving=false;
		// 			Projectile.velocity = Vector3.zero;
		// 			Projectile.angularVelocity = Vector3.zero;
		// 			Projectile.constraints = RigidbodyConstraints.FreezeAll;
		// 			Won = true;
		// 		}
		// 		if(Projectile.GetComponent<Collider>().gameObject.name == "Floor"){
		// 			Debug.Log("Collided With: " + Projectile.GetComponent<Collider>().gameObject.name + " at " + (Projectile.transform.position - InitialPosition).ToString());
		// 			Debug.Log("Collided floor");
		// 			moving=false;
		// 			Projectile.velocity = new Vector3(0f,0f,0f);
		// 			Projectile.constraints = RigidbodyConstraints.FreezeAll;
		// 			Won = false;
		// 		}
		// 	}
		// }	
		
		// if(Projectile.velocity.y == 0){
		// 	Projectile.velocity = Vector3.zero;
		// 	Projectile.angularVelocity = Vector3.zero;
		// }
	}

	void CheckProjectileIsOnTop(){

	}
	void FixedUpdate(){
		Angle_Info.text = Angle.ToString("F2") + "°";
		Speed_Info.text = (Velocity_Mag/100).ToString("F2") + " m/s";
		Vx_Text.text = "Vx: " + Projectile.velocity.z.ToString("F2") + " m/s";
		Vy_Text.text = "Vy: " + Projectile.velocity.y.ToString("F2") + " m/s";
		Max_Height.text = "Hmax: " + max_height.ToString("F2") + " m";
		Height_Text.text = "H: " + (Projectile.transform.position.y - InitialPosition.y).ToString("F2") + "m";
		Distance_Text.text = "D: " + (Projectile.transform.position.z - InitialPosition.z).ToString("F2") + "m";
		TimeInfo.text = "t: " + Timer.ToString("F2");
		TargetDistance.text = "TD: " + (Target.transform.position.z - Projectile.transform.position.z).ToString("F2");
	}
}
