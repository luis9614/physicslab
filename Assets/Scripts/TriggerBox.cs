﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerBox : MonoBehaviour {
	private Vector3 InitialPosition;
	void OnStart(){
		GameObject AuxGO = GameObject.Find("SecondLawController");
		SecondLawController SLC = AuxGO.GetComponent<SecondLawController>();
	 	InitialPosition = SLC.Initial_Position;
	}

	void Update(){
		GameObject AuxGO = GameObject.Find("SecondLawController");
		Debug.Log(AuxGO.ToString());
		SecondLawController SLC = AuxGO.GetComponent<SecondLawController>();
	 	InitialPosition = SLC.Initial_Position;
		Debug.Log(InitialPosition.ToString());
	}
	// Use this for initialization
	void OnTriggerEnter(Collider other){
		other.transform.position = InitialPosition;

		GameObject AuxGO = GameObject.Find("SecondLawController");
		Debug.Log(AuxGO.ToString());
		SecondLawController SLC = AuxGO.GetComponent<SecondLawController>();
	 	InitialPosition = SLC.Initial_Position;
		SLC.Mass.velocity = Vector3.zero;
		SLC.IsAplyingForce = false;
		//Debug.Log("Collision af!");
	}
}
