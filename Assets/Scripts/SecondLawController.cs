﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecondLawController : MonoBehaviour {
	public Rigidbody Mass;
	public GameObject Ramp;
	public PhysicMaterial FrictionData;
	public Vector3 Initial_Position;

	public Slider Angle_Slider;
	public Slider Force_Slider;
	public Slider Mass_Slider;
	public Slider Friction_Slider;

	/**	vARIABLE públicas para mostrar información
	 */
	 public Text Angle_Text;
	 public Text Force_Text;
	 public Text Mass_Text;
	 public Text Friction_Text;


	public bool IsAplyingForce = false;
	private float Desired_Angle = 45;
	private float SFrictionQuotient = 0.2f;
	private float DFrictionQuotient = 0.2f;

	private float Force_Mag = 100;
	private float Mass_Mass = 10;


	private float zscale = 5;
	private float yscale = 0;
	private Vector3 Force;
	// Use this for initialization
	void Start () {
		yscale = zscale*2 * Mathf.Tan(Desired_Angle*Mathf.Deg2Rad);
		Ramp.transform.localScale = new Vector3(3f, yscale, zscale);
		Mass.transform.Rotate(-Desired_Angle,0f,0f);
		Mass.transform.Rotate(-Desired_Angle,0f,0f);
		Mass.transform.position = new Vector3(0f, 5* yscale, -2f);
		Initial_Position = Mass.transform.position;
		FrictionData.dynamicFriction = DFrictionQuotient;
		FrictionData.staticFriction = SFrictionQuotient;
		Mass.GetComponent<Rigidbody>().mass = Mass_Mass;
		Force = new Vector3(0f, Force_Mag * Mathf.Sin(Desired_Angle*Mathf.Deg2Rad),Force_Mag * Mathf.Cos(Desired_Angle*Mathf.Deg2Rad));
		Debug.Log(Mass.transform.rotation.eulerAngles.ToString());
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp("space")){
			//Debug.Log("Space Key Pressed");
			IsAplyingForce = !IsAplyingForce;
		}
		if(IsAplyingForce){
			Mass.AddForce(Force);
		}
		Desired_Angle = Angle_Slider.value;
		yscale = zscale*2 * Mathf.Tan(Desired_Angle*Mathf.Deg2Rad);
		float Force_Mag = Force_Slider.value;

		Ramp.transform.localScale = new Vector3(3f, yscale, zscale);
		if(Mass.transform.rotation.eulerAngles.x != -Desired_Angle){
			float aux = Mass.transform.rotation.eulerAngles.x - (Desired_Angle);
			//Mass.transform.Rotate(-Desired_Angle,0f,0f);
			Mass.transform.Rotate(aux,0f,0f);
		}
			
		//Mass.transform.position = new Vector3(0f, 5* yscale, -2f);
		//Initial_Position = Mass.transform.position;
		FrictionData.dynamicFriction = Friction_Slider.value;
		FrictionData.staticFriction = Friction_Slider.value;
		Mass.GetComponent<Rigidbody>().mass = Mass_Slider.value;
		Force = new Vector3(0f, Force_Mag * Mathf.Sin(Desired_Angle*Mathf.Deg2Rad),Force_Mag * Mathf.Cos(Desired_Angle*Mathf.Deg2Rad));
		
		//Debug.Log(GetMassAngle());
	}

	void FixedUpdate(){
		Angle_Text.text = "Angle: " + Angle_Slider.value.ToString("F2") + "°";
		Force_Text.text = "Force: " + Force_Slider.value.ToString("F2") + "N";
		Mass_Text.text = "Mass: " + Mass_Slider.value.ToString("F2") + "Kg";
		Friction_Text.text = "Friction: " + Friction_Slider.value.ToString("F2");
	}

	string GetMassAngle(){
		return "Mass Angle" + (360 - Mass.transform.rotation.eulerAngles.x).ToString();
	}
}
