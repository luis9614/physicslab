﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFollowingCamera : MonoBehaviour {
	public GameObject Tracking;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.rotation = Quaternion.LookRotation(Tracking.transform.position - transform.position);
	}
}
